#Использовать json
#Использовать logos

Перем Соединение Экспорт;
Перем Запрос Экспорт;
Перем Таблица Экспорт;
Перем Лог Экспорт;

Перем Парсер Экспорт;

Перем АдресПриложения Экспорт;
Перем ПортПриложения Экспорт;
Перем СчетчикДляЗапросаСтатуса Экспорт;

Перем Версия Экспорт;

Перем ИмяЛогаПриложения;
Перем ПутьКФайлуЖурналаПриложения;

Функция Иницилизация() Экспорт
	
	СоздатьТаблицы();

	ИмяЛогаПриложения = "oscript.webapp.tickets";
	ПутьКФайлуЖурналаПриложения = "./wwwroot/logs/webapp.tickets.log";

	Парсер = Новый ПарсерJSON;

	ВыполнитьНастройкиЛогированияПриложения();
	ВыполнитьНастройкиНезасыпания();

	ПолучитьВерсиюПриложения();

	ОбщегоНазначения.Парсер = Парсер;
	ОбщегоНазначения.Лог = Лог;

	МенеджерФоновых.Парсер = Парсер;
	МенеджерФоновых.Лог = Лог;

	Лог.Информация("/// - ЗАПУСК ПРИЛОЖЕНИЯ - ///");

КонецФункции

Функция СоздатьТаблицы() Экспорт

	СоздатьТаблицу();

КонецФункции

Функция СоздатьТаблицу() Экспорт

	Т = Новый ТаблицаЗначений;
	Т.Колонки.Добавить("НомерМатча");
	Т.Колонки.Добавить("Категория");
	Т.Колонки.Добавить("КоличествоСвободныхБилетов");

	Таблица = Т;

КонецФункции

Функция ПолучитьУровеньЛогированияПриложения()

	УровниЛогирования = Новый Соответствие;
	УровниЛогирования.Вставить("DEBUG", 		 УровниЛога.Отладка);
	УровниЛогирования.Вставить("INFO", 			 УровниЛога.Информация);
	УровниЛогирования.Вставить("WARNING", 		 УровниЛога.Предупреждение);
	УровниЛогирования.Вставить("ERROR", 		 УровниЛога.Ошибка);
	УровниЛогирования.Вставить("CRITICAL_ERROR", УровниЛога.КритичнаяОшибка);

	пУровеньЛогирования = ПолучитьПеременнуюСреды("LOGGING_LEVEL");

	Если пУровеньЛогирования = Неопределено Тогда
		пУровеньЛогирования = "DEBUG";
	КонецЕсли;	

	пУровеньЛогирования = УровниЛогирования[пУровеньЛогирования];

	Если пУровеньЛогирования = Неопределено Тогда
		ВызватьИсключение "Не установлен уровень логирования приложения";
	КонецЕсли;

	Возврат пУровеньЛогирования;

КонецФункции

Функция ВыполнитьНастройкиЛогированияПриложения()

	Лог = Логирование.ПолучитьЛог(ИмяЛогаПриложения);

	ФайлЖурнала = Новый ВыводЛогаВФайл;
	ФайлЖурнала.ОткрытьФайл(ПутьКФайлуЖурналаПриложения);
	Лог.ДобавитьСпособВывода(ФайлЖурнала);

	пВыводитьЛогВКонсоль = ПолучитьПеременнуюСреды("LOGGING_CONSOLE");
	Если пВыводитьЛогВКонсоль = Неопределено Тогда
		пВыводитьЛогВКонсоль = Ложь;
	КонецЕсли;

	Если пВыводитьЛогВКонсоль Тогда
		ВыводЛогаВКонсоль = Новый ВыводЛогаВКонсоль;
		Лог.ДобавитьСпособВывода(ВыводЛогаВКонсоль);
	КонецЕсли;	

	пУровеньЛогирования = ПолучитьУровеньЛогированияПриложения();
	Сообщить("Уровень логирования - "+пУровеньЛогирования, СтатусСообщения.Информация);

	Лог.УстановитьУровень(пУровеньЛогирования);

	Лог.УстановитьРаскладку(ЭтотОбъект);

КонецФункции	

Функция Форматировать(Знач Уровень, Знач Сообщение) Экспорт

    Возврат СтрШаблон("%1: %2 - %3", ТекущаяДата(), УровниЛога.НаименованиеУровня(Уровень), Сообщение);

КонецФункции

Процедура ВыполнитьНастройкиНезасыпания()

	СчетчикДляЗапросаСтатуса = 0;

	АдресПриложения = ПолучитьПеременнуюСреды("ADDRESS_APP");
	Если АдресПриложения = Неопределено Тогда
		АдресПриложения = "localhost";
	КонецЕсли;

	Лог.Информация("АДРЕС ПРИЛОЖЕНИЯ - %1", АдресПриложения);

	ПортПриложения = ПолучитьПеременнуюСреды("PORT_APP");
	Если ПортПриложения = Неопределено Тогда
		ПортПриложения = 5000;
	КонецЕсли;

	Лог.Информация("ПОРТ ПРИЛОЖЕНИЯ - %1", ПортПриложения);

КонецПроцедуры

Процедура ПолучитьВерсиюПриложения()

	АдресФайла = "./packagedef";
	Чтение = Новый ЧтениеТекста(АдресФайла, КодировкаТекста.UTF8);
	Текст = Чтение.Прочитать();
	Версия = ВыделитьВерсию(Текст);

	Лог.Информация("ВЕРСИЯ ПРИЛОЖЕНИЯ - %1", Версия);

КонецПроцедуры	

Функция ВыделитьВерсию(Знач Данные)
	
	Лог.Отладка("Применяю регулярку к %1", Данные);
	РЕ = Новый РегулярноеВыражение("(\d{1,3}\.(\d{1,3}\.)*\d{1,3})");
	Совпадения = РЕ.НайтиСовпадения(Данные);
	Если Совпадения.Количество() = 0 Тогда
		Возврат Неопределено;
	КонецЕсли;

	Совпадение = Совпадения[0].Группы[1];
	Возврат Совпадение.Значение;

КонецФункции