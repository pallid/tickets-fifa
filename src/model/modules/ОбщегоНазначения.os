Перем Лог Экспорт;
Перем Парсер Экспорт;

Функция РазобратьПараметрыЗапроса(Знач Параметры) Экспорт

	Результат = Новый Соответствие;

	Пары = СтрРазделить(Сред(Параметры,2), "&");
	Для Каждого Пара Из Пары Цикл
		Поз = СтрНайти(Пара, "=");
		Если Поз <> 0 Тогда
			Ключ = Лев(Пара, Поз-1);
			Значение = Сред(Пара, Поз+1);
			Результат.Вставить(Ключ, Значение);
		КонецЕсли;
	КонецЦикла;

	Возврат Результат;

КонецФункции

Функция ПрочитатьОтветЗапроса(Знач Ответ) Экспорт
	
	ТелоОтвета = Ответ.ПолучитьТелоКакСтроку();
	Если ТелоОтвета = Неопределено Тогда
		ТелоОтвета = "";
	КонецЕсли;

	Лог.Отладка("Код состояния: %1", Ответ.КодСостояния);
	Лог.Отладка("Тело ответа: 
		|%1", ТелоОтвета);

	Результат = Неопределено;
	Если ЗначениеЗаполнено(ТелоОтвета) Тогда
		Результат = Парсер.ПрочитатьJSON(ТелоОтвета);
	КонецЕсли;

	Лог.Отладка("Результат: 
		|%1", Результат);

	Возврат Результат;

КонецФункции